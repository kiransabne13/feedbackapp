//Set up mongoose connection
const mongoose = require('mongoose');

const mongoDB = 'mongodb://kiran:Diamond.1@cluster0-shard-00-00-goias.mongodb.net:27017,cluster0-shard-00-01-goias.mongodb.net:27017,cluster0-shard-00-02-goias.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true';

//const mongoDB = 'mongodb://kiran:Diamond.1@cluster0-shard-00-00-zvmzd.mongodb.net:27017,cluster0-shard-00-01-zvmzd.mongodb.net:27017,cluster0-shard-00-02-zvmzd.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true';
//const mongoDB = 'mongodb://kiran:Diamond.1@cluster0-shard-00-00-k1uil.mongodb.net:27017,cluster0-shard-00-01-k1uil.mongodb.net:27017,cluster0-shard-00-02-k1uil.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true';

mongoose.connect(mongoDB);
mongoose.Promise = global.Promise;

module.exports = mongoose;
