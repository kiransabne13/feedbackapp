const express = require('express');
const logger = require('morgan');
const outlets = require('./routes/outlets');
const users = require('./routes/users');
const waiters = require('./routes/waiters');
const captains = require('./routes/captains');
const customers = require('./routes/customers');
const feedbacks = require('./routes/feedbacks');
const reports = require('./routes/reports');
const currentFeedbacks = require('./routes/currentFeedbacks');
const allFeedbacks = require('./routes/allFeedbacks');
const mobileDetails = require('./routes/mobileDetails');
const bodyParser = require('body-parser');
const mongoose = require('./config/database');

var jwt = require('jsonwebtoken');
const app = express();
var MongoClient = require('mongodb').MongoClient;
//const url = "mongodb://kiran:Diamond.1@cluster0-shard-00-00-k1uil.mongodb.net:27017,cluster0-shard-00-01-k1uil.mongodb.net:27017,cluster0-shard-00-02-k1uil.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true";
const url = "mongodb://kiran:Diamond.1@cluster0-shard-00-00-goias.mongodb.net:27017,cluster0-shard-00-01-goias.mongodb.net:27017,cluster0-shard-00-02-goias.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true";




app.set('secretKey', 'nodeRestApi');

//connection to MongoDB
mongoose.connection.on('error', console.error.bind(console, 'MongoDB connection error:'));


app.use(logger('dev'));
app.use(bodyParser.urlencoded({extended: true}));

app.get('/', function(req,res){
	res.json({"data": "FeedBack Server Staarted"});
});

// public route
app.use('/users', users);

// private route
app.use('/outlets', validateUser, outlets);

app.use('/waiters', validateUser, waiters);

app.use('/captains', validateUser, captains);

app.use('/customers', validateUser, customers);

app.use('/feedbacks', validateUser, feedbacks);

app.use('/allFeedbacks', validateUser, allFeedbacks);

app.use('/getCoinFlag', validateUser, feedbacks);

app.use('/reports', validateUser, reports);

app.use('/currentFeedbacks', validateUser, currentFeedbacks);

app.get('/mobileDetails', validateUser, mobileDetails);



app.get('/favicon.ico', function(req, res) {
		console.log("headers is "+req.headers);
    res.sendStatus(204);
});


app.get('/food', function(req,res){
	console.log(req.headers.startdate + " " + req.headers.enddate);
//	res.json({"data": "FeedBack Server Staarted"});
	let feedbackLists = [];
	MongoClient.connect(url, function(err, db){
		var dbo = db.db("test");
		var query = req.query;
		dbo.collection("feedbacks").find({"simpleDate" : {$gte : req.headers.startdate, $lte : req.headers.enddate}}).toArray(function(err, feedbacks){
			if (err) {
				console.log(err);
			} else {
				console.log("result");

				res.json({status: "success", message: "updated document", data: feedbacks});
				db.close();
			}
		});
	});

});
// count for getting customers
app.get('/customersCount', function(req,res){
	MongoClient.connect(url, function(err, db){
		if (err){
			console.log(err);
		} else {
			var dbo = db.db("test");
			var query = req.query;
			dbo.collection("customers").find({}).count(function(error, result){
				if (error) {
					console.log(error);
				} else {
					console.log(result);

					res.json({status: "success", message: "customers Total Count", data: result});
					db.close();
				}
			});
		}
	});
});


app.get('/birthdays', function(req,res){
	let customersList = [];
	MongoClient.connect(url, function(err, db){
		var dbo = db.db("test");
		var query = req.query;

		var today = new Date();
		var m1 = { "$match" : {  "dob" : { "$exists" : true, "$ne" : '' } } };
		var p1 = {
		    "$project" : {
		        "_id" : 0,
		        "name" : 1,
						"mobile" : 1,
		        "dob" : 1,
		        "todayDayOfYear" : { "$dayOfYear" : today },
		        "dayOfYear" : { "$dayOfYear" : "$dob" }
		    }
		};
		var p2 = {
		    "$project" : {
		        "name" : 1,
		        "dob" : 1,
						"mobile" : 1,
		        "daysTillBirthday" : { "$subtract" : [
		             { "$add" : [
		                     "$dayOfYear",
		             { "$cond" : [{"$lt":["$dayOfYear","$todayDayOfYear"]},365,0 ] }
		             ] },
		             "$todayDayOfYear"
		        ] }
		    }
		};


		    var m2 = { "$match" : {  "daysTillBirthday" : { "$lt" : 1 } } }; // lt:60 = Next 60 days Upcoming Birthdays

		dbo.collection("newCustomers").aggregate([m1, p1, p2, m2]).toArray(function(error, results){
			if (error) {
				console.log(error);
			} else {
				res.json({status: "text", message:"new message", data:results});
			}
		});
	})

});


//anniversary

app.get('/anniversarys', function(req,res){
	let customersList = [];
	MongoClient.connect(url, function(err, db){
		var dbo = db.db("test");
		var query = req.query;

		var today = new Date();
		var m1 = { "$match" : {  "anniversary" : { "$exists" : true, "$ne" : '' } } };
		var p1 = {
		    "$project" : {
		        "_id" : 0,
		        "name" : 1,
						"mobile" : 1,
		        "anniversary" : 1,
		        "todayDayOfYear" : { "$dayOfYear" : today },
		        "dayOfYear" : { "$dayOfYear" : "$anniversary" }
		    }
		};
		var p2 = {
		    "$project" : {
		        "name" : 1,
		        "anniversary" : 1,
						"mobile" : 1,
		        "daysTillBirthday" : { "$subtract" : [
		             { "$add" : [
		                     "$dayOfYear",
		             { "$cond" : [{"$lt":["$dayOfYear","$todayDayOfYear"]},365,0 ] }
		             ] },
		             "$todayDayOfYear"
		        ] }
		    }
		};


		    var m2 = { "$match" : {  "daysTillBirthday" : { "$lt" : 1 } } }; // lt:60 = Next 60 days Upcoming Birthdays

		dbo.collection("newCustomers").aggregate([m1, p1, p2, m2]).toArray(function(error, results){
			if (error) {
				console.log(error);
			} else {
				res.json({status: "text", message:"new message", data:results});
			}
		});
	})

});



app.get('/doof', function(req,res){
    MongoClient.connect(url, function(err,db){
        var dbo = db.db("test");
        dbo.collection("feedback_taken").aggregate([

            {
                $group: {
                    _id: {
                        captainName: "$captainName"
                    },
                    count: { $sum: { $toDouble: "$feedback_count" } }
                }
            },
            {
                $project: {
                    _id: 0,
                    captainName: "$_id.captainName",
                    count: 1,
                }
            }
        ]).toArray(function(error, customers){
            if (error) {
                console.log(error);
            } else {
                res.json({status: "success", message: "getAllCustomers", data: customers});
            }
        });
    });

})


function validateUser(req, res, next) {
  jwt.verify(req.headers['x-access-token'], req.app.get('secretKey'), function(err, decoded) {
    if (err) {
      res.json({status:"error", message: err.message, data:null});
    }else{
      // add user id to request
      req.body.userId = decoded.id;
      next();
    }
  });

}

// express doesn't consider not found 404 as an error so we need to handle 404 explicitly
// handle 404 error
app.use(function(req, res, next) {
 let err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// handle errors
app.use(function(err, req, res, next) {
 console.log(err);

  if(err.status === 404)
   res.status(404).json({message: "Not found"});
  else
    res.status(500).json({message: "Something looks wrong :( !!!"});

});


app.listen(3000, function(){
	console.log('Node running on posrt 3000');
});
