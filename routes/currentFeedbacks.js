const express = require('express');
const router = express.Router();
const currentFeedbackController = require('../app/api/controllers/currentFeedbacks');

router.get('/', currentFeedbackController.getAll);

module.exports = router;
