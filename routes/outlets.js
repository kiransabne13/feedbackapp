const express = require('express');
const router = express.Router();
const outletController = require('../app/api/controllers/outlets');

router.get('/', outletController.getAll);
router.post('/', outletController.create);
router.get('/:outletId', outletController.getById);
router.put('/:outletId', outletController.updateById);
router.delete('/:outletId', outletController.deleteById);

module.exports = router;
