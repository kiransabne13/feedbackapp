const express = require('express');
const router = express.Router();
const captainController = require('../app/api/controllers/captains');

router.get('/', captainController.getAll);
router.post('/', captainController.create);
router.get('/:captainId', captainController.getById);
router.put('/:captainId', captainController.updateById);
router.delete('/:captainId', captainController.deleteById);

module.exports = router;
