const express = require('express');
const router = express.Router();
const allFeedbacksController = require('../app/api/controllers/allFeedbacks');

router.get('/', allFeedbacksController.getAll);

module.exports = router;
