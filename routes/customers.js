const express = require('express');
const router = express.Router();
const customerController = require('../app/api/controllers/customers');


router.get('/', customerController.getAll);
router.post('/', customerController.create);
router.get('/:customerId', customerController.getById);
router.put('/:customerId', customerController.updateById);
router.delete('/:customerId', customerController.deleteById);

module.exports = router;
