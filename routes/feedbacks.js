const express = require('express');
const router = express.Router();
const feedbackController = require('../app/api/controllers/feedbacks');

router.get('/', feedbackController.getAll);
router.post('/', feedbackController.create);
router.get('/:feedbackId', feedbackController.getById);
router.put('/:feedbackId', feedbackController.updateById);
router.delete('/:feedbackId', feedbackController.deleteById);
router.get('/getCoinFlag', feedbackController.getCoinFlag);
router.get('/:coinFlag', feedbackController.getCoin);
//router.get('/dates', feedbackController.getByDate); //just added

module.exports = router;
