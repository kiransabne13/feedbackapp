const express = require('express');
const router = express.Router();
const reportsController = require('../app/api/controllers/reports');

router.get('/', reportsController.getAll);

module.exports = router;
