const express = require('express');
const router = express.Router();
const waiterController = require('../app/api/controllers/waiters');

router.get('/', waiterController.getAll);
router.post('/', waiterController.create);
router.get('/:waiterId', waiterController.getById);
router.put('/:waiterId', waiterController.updateById);
router.delete('/:waiterId', waiterController.deleteById);

module.exports = router;
