const express = require('express');
const router = express.Router();
const mobileDetailsModel = require('../app/api/controllers/mobileDetails');

router.get('/', mobileDetailsModel.getAll);

module.exports = router;
