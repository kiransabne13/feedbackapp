const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const WaiterSchema = new Schema ({
  waiterName: {
    type: String,
    required:true,
    trim: true,
    default: '0'
  },
  waiterNo: {
    type: Number,
    trim: true,
    default: 0
  },
  restaurantName: {
    type: String,
    required: true,
    trim: true,
    default: '0'
  }
});

module.exports = mongoose.model('Waiter', WaiterSchema);
