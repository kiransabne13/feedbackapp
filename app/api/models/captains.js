const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const CaptainSchema = new Schema ({
  captainName: {
    type: String,
    trim: true,
    required: true,
  },
  captainNumber: {
    type: Number,
    trim: true,
    required: true,
    default: 0
  },
  restaurantName: {
    type: String,
    trim: true,
    required: true,
    default: 'GoldCoin Fine Dine'
  }


});

module.exports = mongoose.model('Captain', CaptainSchema);
