const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const customerSchema = new Schema({
  name: {
    type: String,
    required: true,
    trim: true,
    default: '0',
  },
  mobile: {
    type: Number,
    required: true,
    trim: true,
    default: 0
  },
  email: {
    type: String,
    trim: true,
    default: '0'
  },
  gender: {
    type: String,
    trim: true,
    default: '0'
  },
  dateOfBirth: {
    type: String,
    trim: true,
    required: true,
    default: '0'
  },
  dateOfAnn: {
    type: String,
    trim: true,
    default:'0'
  },
  lastVisit: {
    type: String,
    trim: true,
    default: '0'
  },
  visitCount: {
    type: Number,
    trim: true,
  },
  sales: {
    type: Number,
    trim: true,
    default: 0,
  },
  points:{
    type: String,
    default: '0'
  },
  blacklist: {
    type: Number,
    default: 0,
    trim: true
  },
  source: {
    type: String,
    default: "STORE",
    trim: true
  },
  flex_1:{
    type: String,
    default: '0',
    trim: true
  },
  flex_2:{
    type: String,
    default: '0',
    trim: true
  },
  flex_3:{
    type: String,
    default: '0',
    trim: true
  },
  flex_4: {
    type: String,
    default: '0',
    trim: true
  },
  dnd: {
    type: String,
    default: '0',
    trim: true
  },
  //default excel import
  created: {
    type: Number,
    default: 0,
    trim: true
  },
  //created_on is new data for input of creation
  created_on:{
    type: Date,
    trim: true
  },
  customer_type: {
    type: String,
    default: 'CUSTOMER',
    trim: true
  },
  last_visit: {
    type: Number,
    default: 0
  },
  total_visit: {
    type: Number,
    default: 0,
    trim: true
  },
  total_sale: {
    type: Number,
    default: 0,
    trim: true
  }


});

module.exports = mongoose.model('Customer', customerSchema);
