const mongoose = require('mongoose');

//define Schema
const Schema = mongoose.Schema;

const OutletSchema = new Schema({
  name: {
    type: String,
    trim: true,
    required: true,
    default: '0'
  },
  created_on: {
    type: Date,
    trim: true,

  },
  waiterCount: {
    type: Number,
    default: 0
  },
  captainCount: {
    type: Number,
    default: 0
  }
});

module.exports = mongoose.model('Outlet', OutletSchema);
