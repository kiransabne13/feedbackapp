
const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const FeedbackSchema = new Schema ({

  restaurantName: {
    type: String,
    required: true,
    trim: true,
    default: 'Gold Coin Fine Dine',
  },
  tableNumber: {
    type: Number,
    required: true,
    trim: true,
    default: 0,
  },
  billNumber:{
    type: Number,
    required: true,
    trim: true,
    default: 0,
  },
  captainName:{
    type: String,
    required: true,
    trim: true,
    default: 'captain one',
  },
  waiterName: {
    type: String,
    trim: true,
    default: 'waiter one',
  },
  customerName:{
    type: String,
    required: true,
    trim: true,
    default: 'customer one',
  },
  customerMobile:{
    type: String,
    required: true,
  //  unique: true,
    trim: true,
    default: '0000000000',
  },
  email:{
    type: String,
    trim: true,
    default: 'a@a.com',
  },
  dateOfBirth:{
    type: String,
    trim: true,
    required: true,
    default: '0',
  },
  dateOfAnn:{
    type: String,
    trim: true,
    required: true,
    default: '0',
  },
  ambiancePoints:{
    type: Number,
    trim: true,
    default: 0
  },
  ambianceRemarks: {
    type: String,
    trim: true,
    default: '0'
  },
  foodQualityPoints: {
    type: Number,
    trim: true,
    default: 0
  },
  foodQualityRemarks:{
    type: String,
    trim: true,
    default: '0'
  },
  customerServicePoints:{
    type: Number,
    trim: true,
    default: 0
  },
  customerServiceRemarks:{
    type: String,
    trim: true,
    default: '0'
  },
  staffBehaviourPoints:{
    type: Number,
    trim: true,
    default: 0
  },
  staffBehaviourRemarks:{
    type: String,
    trim: true,
    default: '0'
  },
  valueForMoneyPoints:{
    type: Number,
    trim: true,
    default: 0
  },
  valueForMoneyRemarks:{
    type: String,
    trim: true,
    default: '0'
  },
  suggestion:{
    type: String,
    trim: true,
    default: '0'
  },
  date:{
    type: Date,
    default: Date.now
  },
  coinFlag: {
    type: String,
    trim: true,
    default: '0'
  },
  simpleDate: {
    type: String,
    trim: true,
    default: '0',
  },
  currentDate: {
    type: String,
    trim: true,
    default: '0'
  }
});

module.exports = mongoose.model('Feedback', FeedbackSchema);
