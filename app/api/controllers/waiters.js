const waiterModel = require('../models/waiters');

module.exports = {

  getById: function(req,res,next){
      console.log(req.body);
      waiterModel.findById(req.params.waiterId, function(err, waiterInfo){
        if (err) {
          next(err);

        } else {
          res.json({status: "success", message: "Found Waiter", data: {waiterInfo: waiterInfo}});
        }
      });
  },


  //get All waiters
  getAll: function(req,res,next){
    let waiterLists = [];

    waiterModel.find({}, function(err, waiters){

      if (err) {
        next(err);

      } else {
        for (let waiter of waiters) {
          waiterLists.push({id: waiter._id, name: waiter.waiterName, waiterNo: waiter.waiterNo, restaurantName: waiter.restaurantName});

        }
        res.json({status:"success", message: "Get All Outlets", data: {waiters: waiterLists}});

      }
    });
  },

  //get All waiters by restaurantName

//  updateById

  updateById: function(req,res,next){
    waiterModel.findByIdAndUpdate(req.params.waiterId, {name:req.body.waiterName}, function(err, waiterInfo){

    if (err) {
      next(err);
    }  else {
      res.json({status: "success", message: "Waiter Update Successful", data: null});

      }
    });
  },

  deleteById: function(req,res,next){
    waiterModel.findByIdAndRemove(req.params.waiterId, function(err, waiterInfo){
      if (err) {
          next(err);
      } else {
        res.json({status:"success", message: "deleted successgully", data: null});
      }
    });
  },


  create: function(req,res,next){
      waiterModel.create({waiterName: req.body.waiterName, restaurantName: req.body.restaurantName, waiterNo: req.body.waiterNo}, function (err, result){
        if (err) {
            next(err);
        } else {
          res.json({status: "success", message: "Waiter Added", data: null});
        }
      });
  },

}
