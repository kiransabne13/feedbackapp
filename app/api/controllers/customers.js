const customerModel = require('../models/customers');
const MongoClient = require('mongodb').MongoClient;

//const url = "mongodb://kiran:Diamond.1@cluster0-shard-00-00-k1uil.mongodb.net:27017,cluster0-shard-00-01-k1uil.mongodb.net:27017,cluster0-shard-00-02-k1uil.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true";
const url = "mongodb://kiran:Diamond.1@cluster0-shard-00-00-goias.mongodb.net:27017,cluster0-shard-00-01-goias.mongodb.net:27017,cluster0-shard-00-02-goias.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true";

//const url = "mongodb://kiran:Diamond.1@cluster0-shard-00-00-k1uil.mongodb.net:27017,cluster0-shard-00-01-k1uil.mongodb.net:27017,cluster0-shard-00-02-k1uil.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true";
//const url = "mongodb://kiran:Diamond.1@cluster0-shard-00-00-zvmzd.mongodb.net:27017,cluster0-shard-00-01-zvmzd.mongodb.net:27017,cluster0-shard-00-02-zvmzd.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true";

module.exports = {

  getById: function(req,res,next){
    console.log(req.body);
    customerModel.findById(req.params.customerId, function(err, customerInfo){
      if (err) {
        next(err);
      } else {
        res.json({status: "success", message: "Found Customer", data: {customers: customerInfo}});
      }
    });
  },


   getAll: function(req,res,next){
       let customersList = [];

     customerModel.find(req.query, function(err, customers){

       if (err) {
         next(err);
       } else {
         for (let customer of customers) {
           customersList.push({id: customer._id, name: customer.name, mobile: Number(customer.mobile), email: customer.email, gender: customer.gender, dateOfBirth: customer.dateOfBirth, dateOfAnn: customer.dateOfAnn, lastVisit: Number(customer.lastVisit), visitCount: Number(customer.visitCount), sales: customer.sales, points: customer.points, total_sale: Number(customer.total_sale)});
         }
         console.log(customersList);
         res.json({status:"success", message: "Get All Customers", data: {customers: customersList}});
         }
     });
   },

  //getAll: function(req,res,next){
    //MongoClient.connect(url, function(err,db){
      //var dbo = db.db("test");
  //    dbo.collection("customers").find({}).toArray(function(error, customers){
    //    if (error) {
      //    console.log(error);
        //} else {
          //res.json({status: "success", message: "getAllCustomers", data: customers});
  //      }
    //  });
  //  });
//  },

  updateById: function(req,res,next){

     MongoClient.connect(url, function (err,db){
       var myquery = { 'mobile': Number(req.body.mobile) };
       var updateQuery = { 'customerMobile': req.body.mobile};
       var newvalues = { $set: {'sales': Number(req.body.sales), 'points': req.body.points, 'total_sale': Number(req.body.total_sale)} };
       var updateFlag = { $set: {'coinFlag': 'Set'}};
       var dbo = db.db("test");
       dbo.collection("customers").findOneAndUpdate(myquery, newvalues, function(err, result) {

         if (err) {
           next(err);
         } else {
           console.log("1 document updated");
           res.json({status: "success", message: "updated document", data: result});

           db.close();
         }
       });

       dbo.collection("feedbacks").findOneAndUpdate(updateQuery, updateFlag, function(err, ress) {

         if (err) {
           next(err);
           return;
         } else {
           console.log("1 document updated");
        //   res.json({status: "success", message: "updated document", data: ress});

           db.close();
         }
       });

     });
     console.log(req.body.name);
     console.log(req.body.rating +" type of is "+ typeof(Number(req.body.rating)));

   },


  deleteById: function(req,res,next){
    customerModel.findByIdAndRemove(req.params.customerId, function(err, customerInfo){
      if (err) {
          next(err);
      } else {
        res.json({status:"success", message: "deleted successgully", data: null});
      }
    });
  },


  create: function(req,res,next){
    /*
      customerModel.create({name: req.body.name, mobile: Number(req.body.mobile), email: req.body.email, gender: req.body.gender, dateOfBirth: req.body.dateOfBirth, dateOfAnn: req.body.dateOfAnn, lastVisit: Number(req.body.lastVisit), visitCount: Number(req.body.visitCount), sales: Number(req.body.sales), points: req.body.points, dob: req.body.dob, anniversary: req.body.anniversary, blacklist: Number(req.body.blacklist), source: req.body.source, flex_1: req.body.flex_1, flex_2: req.body.flex_2, flex_3: req.body.flex_3, flex_4: req.body.flex_4, dnd: req.body.dnd, created_on: new Date(Date.now()), customer_type: "CUSTOMER", total_visit: Number(req.body.total_visit), total_sale: Number(req.body.total_sale) }, function (err, result){
        if (err) {
            next(err);
        } else {
          res.json({status: "success", message: "Customer Added", data: {id: result._id, mobile: result.mobile}});
        }
      });
      */
      MongoClient.connect(url, function(err,db){
        if (err) {
          throw err;
        }
        var dbo = db.db("test");
        dbo.collection("customers").updateMany({mobile: Number(req.body.mobile)}, {$set:{name: req.body.name, email: req.body.email, gender: req.body.gender, dateOfBirth: req.body.dateOfBirth, dateOfAnn: req.body.dateOfAnn, lastVisit: Number(req.body.lastVisit), visitCount: Number(1), sales: Number(req.body.sales), points: req.body.points, dob: req.body.dob, anniversary: req.body.anniversary, blacklist: Number(req.body.blacklist), source: req.body.source, flex_1: req.body.flex_1, flex_2: req.body.flex_2, flex_3: req.body.flex_3, flex_4: req.body.flex_4, dnd: req.body.dnd, created_on: new Date(Date.now()), customer_type: "CUSTOMER", total_visit: Number(1), total_sale: Number(req.body.total_sale)}}, {upsert: true}, function(err, result){
          if (err) {
            console.log(err);
          } else {
            res.json({status: "success", message: "Customer Added", data: result});
          }
        })
      })
  },




}
