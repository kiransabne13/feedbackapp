const feedbackModel = require('../models/feedbacks');
var moment = require('moment');
var MongoClient = require('mongodb').MongoClient;

//const url = "mongodb://kiran:Diamond.1@cluster0-shard-00-00-k1uil.mongodb.net:27017,cluster0-shard-00-01-k1uil.mongodb.net:27017,cluster0-shard-00-02-k1uil.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true";
const url = "mongodb://kiran:Diamond.1@cluster0-shard-00-00-goias.mongodb.net:27017,cluster0-shard-00-01-goias.mongodb.net:27017,cluster0-shard-00-02-goias.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true";

//const url = "mongodb://kiran:Diamond.1@cluster0-shard-00-00-k1uil.mongodb.net:27017,cluster0-shard-00-01-k1uil.mongodb.net:27017,cluster0-shard-00-02-k1uil.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true";
//const url = "mongodb://kiran:Diamond.1@cluster0-shard-00-00-zvmzd.mongodb.net:27017,cluster0-shard-00-01-zvmzd.mongodb.net:27017,cluster0-shard-00-02-zvmzd.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true";

module.exports = {

  getById: function(req,res,next){
    console.log(req.body);
    feedbackModel.findById(req.params.feedbackId, function(err, feedbackInfo){
      if (err) {
        next(err);
      } else {
        res.json({status: "Success", message:" Found Feedback", data: {feedbacks: feedbackInfo}});
      }
    });
  },

  getCoin: function(req,res,next){
    console.log("way out");
    feedbackModel.find({}, function(err, feedbackInfo){

    });


  },



  // getAll: function(req,res,next){
  //   let feedbackLists = [];
  //   console.log(req.query);
  //   feedbackModel.find({"date": {$gte: new Date((new Date().getTime() - (1 * 24 * 60 * 60 * 1000)))}},{},{sort: {'date':-1}}, function(err, feedbacks){
  //     if (err) {
  //       next(err);
  //     } else {
  //       for (let feedback of feedbacks){
  //         feedbackLists.push({id: feedback._id, restaurantName: feedback.restaurantName, tableNumber: feedback.tableNumber, billNumber: feedback.billNumber, captainName: feedback.captainName, waiterName: feedback.waiterName, customerName: feedback.customerName, customerMobile: feedback.customerMobile, email: feedback.email, dateOfBirth: feedback.dateOfBirth, dateOfAnn: feedback.dateOfAnn, ambiancePoints: feedback.ambiancePoints, ambianceRemarks: feedback.ambianceRemarks, foodQualityPoints: feedback.foodQualityPoints, foodQualityRemarks: feedback.foodQualityRemarks, customerServicePoints: feedback.customerServicePoints, customerServiceRemarks: feedback.customerServiceRemarks, staffBehaviourPoints: feedback.staffBehaviourPoints, staffBehaviourRemarks: feedback.staffBehaviourRemarks, valueForMoneyPoints:feedback.valueForMoneyPoints, valueForMoneyRemarks: feedback.valueForMoneyRemarks, suggestion: feedback.suggestion, date: feedback.date, coinFlag: feedback.coinFlag, simpledate: feedback.simpledate, currentDate: feedback.currentDate });
  //       }
  //       res.json({status:"success", message:"get All feedbacks", data: {feedbacks: feedbackLists}});
  //     }
  //   });
  // //else ends here
  //
  // },

  getAll: function(req,res,next){
    MongoClient.connect(url, function(err, db){
      if (err) {
        console.log(err);
      } else {
        var dbo = db.db("test");
        dbo.collection("feedbacks").find({"date": {$gte: new Date((new Date().getTime() - (4 * 24 * 60 * 60 * 1000)))}}).sort({date : -1}).toArray(function(error, results){
          if (error) {
            throw error;
          } else {
            res.json({status: "success", message: "get All Feedbacks", data : results});
          }
        });
      }
    });
  },

  getByDate: function(req,res,next){
    let feedbackLists = [];
    console.log(req.query);
    feedbackModel.find({}, function(err, feedbacks){
      if (err) {
        console.log(err);
      } else {
        for (let feedback of feedbacks){
          feedbackLists.push({id: feedback._id, restaurantName: feedback.restaurantName, tableNumber: feedback.tableNumber, billNumber: feedback.billNumber, captainName: feedback.captainName, waiterName: feedback.waiterName, customerName: feedback.customerName, customerMobile: feedback.customerMobile, email: feedback.email, dateOfBirth: feedback.dateOfBirth, dateOfAnn: feedback.dateOfAnn, ambiancePoints: feedback.ambiancePoints, ambianceRemarks: feedback.ambianceRemarks, foodQualityPoints: feedback.foodQualityPoints, foodQualityRemarks: feedback.foodQualityRemarks, customerServicePoints: feedback.customerServicePoints, customerServiceRemarks: feedback.customerServiceRemarks, staffBehaviourPoints: feedback.staffBehaviourPoints, staffBehaviourRemarks: feedback.staffBehaviourRemarks, valueForMoneyPoints:feedback.valueForMoneyPoints, valueForMoneyRemarks: feedback.valueForMoneyRemarks, suggestion: feedback.suggestion, date: feedback.date, coinFlag: feedback.coinFlag, simpledate: feedback.simpledate, currentDate: feedback.currentDate });
        }
        res.json({status: "success", message: "get All by Date", data: {feedbacks: feedbackLists}});
      }
    });
  },


  updateById: function(req,res,next){
    feedbackModel.findByIdAndUpdate(req.params.feedbackId, {restaurantName:req.body.restaurantName, tableNumber: req.body.tableNumber, billNumber: req.body.billNumber, captainName: req.body.captainName, waiterName: req.body.waiterName, customerName: req.body.customerName, customerMobile: req.body.customerMobile, email: req.body.email, dateOfBirth: req.body.dateOfBirth, dateOfAnn: req.body.dateOfAnn, ambiancePoints: req.body.ambiancePoints, ambianceRemarks: req.body.ambianceRemarks, foodQualityPoints: req.body.foodQualityPoints, foodQualityRemarks: req.body.foodQualityRemarks, customerServicePoints: req.body.customerServicePoints, customerServiceRemarks: req.body.customerServiceRemarks, staffBehaviourPoints: req.body.staffBehaviourPoints, staffBehaviourRemarks: req.body.staffBehaviourRemarks, valueForMoneyPoints: req.body.valueForMoneyPoints, valueForMoneyRemarks: req.body.valueForMoneyRemarks, suggestion: req.body.suggestion, date: req.body.date, coinFlag: req.body.coinFlag }, function(err, feedbackInfo){
      if (err) {
        next(err);
      } else {
        res.json({status: "success", message: "Feedback Update Successful", data: null});
      }
    });
  },

  deleteById: function(req,res,next){
    feedbackModel.findByIdAndRemove(req.params.feedbackId, function(err, feedbackInfo){
      if (err){
        next(err);
      } else {
        res.json({status: "success", message: "deleted by Id", data: null});
      }
    });
  },
/*
  create: function(req,res,next){
    feedbackModel.create({restaurantName:req.body.restaurantName, tableNumber: req.body.tableNumber, billNumber: req.body.billNumber, captainName: req.body.captainName, waiterName: req.body.waiterName, customerName: req.body.customerName, customerMobile: req.body.customerMobile, email: req.body.email, dateOfBirth: req.body.dateOfBirth, dateOfAnn: req.body.dateOfAnn, ambiancePoints: req.body.ambiancePoints, ambianceRemarks: req.body.ambianceRemarks, foodQualityPoints: req.body.foodQualityPoints, foodQualityRemarks: req.body.foodQualityRemarks, customerServicePoints: req.body.customerServicePoints, customerServiceRemarks: req.body.customerServiceRemarks, staffBehaviourPoints: req.body.staffBehaviourPoints, staffBehaviourRemarks: req.body.staffBehaviourRemarks, valueForMoneyPoints: req.body.valueForMoneyPoints, valueForMoneyRemarks: req.body.valueForMoneyRemarks, suggestion: req.body.suggestion, coinFlag: req.body.coinFlag, simpleDate: req.body.simpleDate, currentDate: req.body.currentDate}, function(err, result){
      if (err) {
        next(err);
      } else {
        res.json({status: "success", message: "feedback Added", data:{id: result._id, billNumber: result.billNumber}});
      }
    });
  },
*/
// trying to use raw uploads


//   create: function(req,res,next){
//
//     MongoClient.connect(url, function(err, db) {
//       if (err) throw err;
//       var dbo = db.db("test");
//       var myobj = { name: "Company Inc", address: "Highway 37" };
//       var values = {restaurantName:req.body.restaurantName, tableNumber: Number(req.body.tableNumber), billNumber: Number(req.body.billNumber), captainName: req.body.captainName, waiterName: req.body.waiterName, customerName: req.body.customerName, customerMobile: req.body.customerMobile, email: req.body.email, dateOfBirth: req.body.dateOfBirth, dateOfAnn: req.body.dateOfAnn, ambiancePoints: Number(req.body.ambiancePoints), ambianceRemarks: req.body.ambianceRemarks, foodQualityPoints: Number(req.body.foodQualityPoints), foodQualityRemarks: req.body.foodQualityRemarks, customerServicePoints: Number(req.body.customerServicePoints), customerServiceRemarks: req.body.customerServiceRemarks, staffBehaviourPoints: Number(req.body.staffBehaviourPoints), staffBehaviourRemarks: req.body.staffBehaviourRemarks, valueForMoneyPoints: Number(req.body.valueForMoneyPoints), valueForMoneyRemarks: req.body.valueForMoneyRemarks, suggestion: req.body.suggestion, coinFlag: req.body.coinFlag, simpleDate: req.body.simpleDate, currentDate: req.body.currentDate, date: new Date((new Date().getTime()))};
//
//       var newValues = {restaurantName:req.body.restaurantName, tableNumber: Number(req.body.tableNumber), billNumber: Number(req.body.billNumber), captainName: req.body.captainName, waiterName: req.body.waiterName, customerName: req.body.customerName, customerMobile: req.body.customerMobile, email: req.body.email, dateOfBirth: req.body.dateOfBirth, dateOfAnn: req.body.dateOfAnn, ambiancePoints: Number(req.body.ambiancePoints), ambianceRemarks: req.body.ambianceRemarks, foodQualityPoints: Number(req.body.foodQualityPoints), foodQualityRemarks: req.body.foodQualityRemarks, customerServicePoints: Number(req.body.customerServicePoints), customerServiceRemarks: req.body.customerServiceRemarks, staffBehaviourPoints: Number(req.body.staffBehaviourPoints), staffBehaviourRemarks: req.body.staffBehaviourRemarks, valueForMoneyPoints: Number(req.body.valueForMoneyPoints), valueForMoneyRemarks: req.body.valueForMoneyRemarks, suggestion: req.body.suggestion, coinFlag: req.body.coinFlag, simpleDate: req.body.simpleDate, currentDate: req.body.currentDate, dayFlag: req.body.dayFlag, date: new Date((new Date().getTime()))};
//       var myquery = {'mobile': req.body.mobile };
//       var updateFlag = { $inc: {'visitCount': Number(1)}};
//
// //
//       dbo.collection("feedbacks").updateMany({billNumber: Number(req.body.billNumber), simpleDate: req.body.simpleDate, currentDate: req.body.currentDate}, {$set:{restaurantName: req.body.restaurantName, tableNumber: Number(req.body.tableNumber), captainName: req.body.captainName, waiterName: req.body.waiterName, customerName: req.body.customerName, customerMobile: req.body.customerMobile, email: req.body.email, dateOfBirth: req.body.dateOfBirth, dateOfAnn: req.body.dateOfAnn, ambiancePoints: Number(req.body.ambiancePoints), ambianceRemarks: req.body.ambianceRemarks, foodQualityPoints: Number(req.body.foodQualityPoints), foodQualityRemarks: req.body.foodQualityRemarks, customerServicePoints: Number(req.body.customerServicePoints), customerServiceRemarks: req.body.customerServiceRemarks, staffBehaviourPoints: Number(req.body.staffBehaviourPoints), staffBehaviourRemarks: req.body.staffBehaviourRemarks, valueForMoneyPoints: Number(req.body.valueForMoneyPoints), valueForMoneyRemarks: req.body.valueForMoneyRemarks, suggestion: req.body.suggestion, coinFlag: req.body.coinFlag, date: new Date((new Date().getTime()))}}, {upsert: true}, function(err, result) {
//
//         if (err) {
//           console.log(err);
//         } else {
//           console.log("1 document inserted");
//           res.json({status: "success", message: "updated document", data: req.body.billNumber});
//           db.close();
//         }
//
//       });
//       //adding in dayFeedbacksSchema
// //      dbo.collection("dayFeedback").updateMany({billNumber: Number(req.body.billNumber), simpleDate: req.body.simpleDate, currentDate: req.body.currentDate}, {
//       dbo.collection("dayFeedback").updateMany({billNumber: Number(req.body.billNumber), simpleDate: req.body.simpleDate, currentDate: req.body.currentDate}, {$set:{restaurantName: req.body.restaurantName, tableNumber: Number(req.body.tableNumber), captainName: req.body.captainName, waiterName: req.body.waiterName, customerName: req.body.customerName, customerMobile: req.body.customerMobile, email: req.body.email, dateOfBirth: req.body.dateOfBirth, dateOfAnn: req.body.dateOfAnn, ambiancePoints: Number(req.body.ambiancePoints), ambianceRemarks: req.body.ambianceRemarks, foodQualityPoints: Number(req.body.foodQualityPoints), foodQualityRemarks: req.body.foodQualityRemarks, customerServicePoints: Number(req.body.customerServicePoints), customerServiceRemarks: req.body.customerServiceRemarks, staffBehaviourPoints: Number(req.body.staffBehaviourPoints), staffBehaviourRemarks: req.body.staffBehaviourRemarks, valueForMoneyPoints: Number(req.body.valueForMoneyPoints), valueForMoneyRemarks: req.body.valueForMoneyRemarks, suggestion: req.body.suggestion, dayFlag: req.body.dayFlag, coinFlag: req.body.coinFlag, date: new Date((new Date().getTime()))}}, {upsert: true}, function(err, ress){
//         if (err) {
//           console.log(err);
//         } else {
//           console.log("1 dayFeedback added");
//         //  db.customers.update({ mobile: req.body.customerMobile },{ $inc: { visitCount: 1 } });
//
//           db.close();
//         }
//       });
//
//       dbo.collection("customers").updateMany({mobile: Number(req.body.customerMobile)}, { $inc: {visitCount: 1}}, {upsert: true}, function(error, resu){
//         if (error) {
//           console.log(error);
//         } else{
//           console.log("done");
//         }
//       })
//
// //      dbo.collection("customers").updateOne({mobile: Number(req.body.customerMobile), email: req.body.email, name: req.body.customerName, gender: "", dob: req.body.dateOfBirth, anniversary: req.body.dateOfAnn, blacklist: Number(0), source: "STORE", flex_1: "", flex_2: "", flex_3: "", flex_4: "", dnd: "NoDnd", whitelisted: Number(0), created: Number(0), customer_type: "CUSTOMER", customer_address:"", comments: "", last_visit: Number(0), total_sale: Number(0) }, {$inc:{ visitCount: Number(1)}},{upsert: true}, function(err, result) {
//
//   //      if (err) {
//     //      next(err);
//       //  } else {
//         //  console.log("1 customer updated");
//         //  res.json({status: "success", message: "updated document", data: result});
//
//  //         db.close();
//    //     }
//      // });
//       //dbo increment the visitCount Counter
//
//   //  dbo.collection("customers").findOneAndUpdate(updateQuery, updateFlag, function(err, ress) {
//
//     });
//   },


  getCoinFlag: function(req,res,next){
    let feedbackLists = [];
    console.log(req.body);
    feedbackModel.find({}, function(err, feedbacks){
      if (err) {
        next(err);
      } else {
        for (let feedback of feedbacks){
          feedbackLists.push({id: feedback._id, restaurantName: feedback.restaurantName, tableNumber: feedback.tableNumber, billNumber: feedback.billNumber, captainName: feedback.captainName, waiterName: feedback.waiterName, customerName: feedback.customerName, customerMobile: feedback.customerMobile, email: feedback.email, dateOfBirth: feedback.dateOfBirth, dateOfAnn: feedback.dateOfAnn, ambiancePoints: feedback.ambiancePoints, ambianceRemarks: feedback.ambianceRemarks, foodQualityPoints: feedback.foodQualityPoints, foodQualityRemarks: feedback.foodQualityRemarks, customerServicePoints: feedback.customerServicePoints, customerServiceRemarks: feedback.customerServiceRemarks, staffBehaviourPoints: feedback.staffBehaviourPoints, staffBehaviourRemarks: feedback.staffBehaviourRemarks, valueForMoneyPoints:feedback.valueForMoneyPoints, valueForMoneyRemarks: feedback.valueForMoneyRemarks, suggestion: feedback.suggestion, date: feedback.date, coinFlag: feedback.coinFlag, simpledate: feedback.simpledate, currentDate: feedback.currentDate });
        }
        res.json({status:"success", message:"get All feedbacks", data: {feedbacks: feedbackLists}});
      }
    });

  },

  //making changes in the process, on create on feedback , it would first create a feedback and upsert, then add in dayfeedback collection , then updates customers collections, now ill also upsert the feedback_taken collections
  create: function(req,res,next){

     MongoClient.connect(url, async function(err, db) {
      if (err) throw err;
      var dbo = db.db("test");
      var myobj = { name: "Company Inc", address: "Highway 37" };
      var values = {restaurantName:req.body.restaurantName, tableNumber: Number(req.body.tableNumber), billNumber: Number(req.body.billNumber), captainName: req.body.captainName, waiterName: req.body.waiterName, customerName: req.body.customerName, customerMobile: req.body.customerMobile, email: req.body.email, dateOfBirth: req.body.dateOfBirth, dateOfAnn: req.body.dateOfAnn, ambiancePoints: Number(req.body.ambiancePoints), ambianceRemarks: req.body.ambianceRemarks, foodQualityPoints: Number(req.body.foodQualityPoints), foodQualityRemarks: req.body.foodQualityRemarks, customerServicePoints: Number(req.body.customerServicePoints), customerServiceRemarks: req.body.customerServiceRemarks, staffBehaviourPoints: Number(req.body.staffBehaviourPoints), staffBehaviourRemarks: req.body.staffBehaviourRemarks, valueForMoneyPoints: Number(req.body.valueForMoneyPoints), valueForMoneyRemarks: req.body.valueForMoneyRemarks, suggestion: req.body.suggestion, coinFlag: req.body.coinFlag, simpleDate: req.body.simpleDate, currentDate: req.body.currentDate, date: new Date((new Date().getTime()))};

      var newValues = {restaurantName:req.body.restaurantName, tableNumber: Number(req.body.tableNumber), billNumber: Number(req.body.billNumber), captainName: req.body.captainName, waiterName: req.body.waiterName, customerName: req.body.customerName, customerMobile: req.body.customerMobile, email: req.body.email, dateOfBirth: req.body.dateOfBirth, dateOfAnn: req.body.dateOfAnn, ambiancePoints: Number(req.body.ambiancePoints), ambianceRemarks: req.body.ambianceRemarks, foodQualityPoints: Number(req.body.foodQualityPoints), foodQualityRemarks: req.body.foodQualityRemarks, customerServicePoints: Number(req.body.customerServicePoints), customerServiceRemarks: req.body.customerServiceRemarks, staffBehaviourPoints: Number(req.body.staffBehaviourPoints), staffBehaviourRemarks: req.body.staffBehaviourRemarks, valueForMoneyPoints: Number(req.body.valueForMoneyPoints), valueForMoneyRemarks: req.body.valueForMoneyRemarks, suggestion: req.body.suggestion, coinFlag: req.body.coinFlag, simpleDate: req.body.simpleDate, currentDate: req.body.currentDate, dayFlag: req.body.dayFlag, date: new Date((new Date().getTime()))};
      var myquery = {'mobile': req.body.mobile };
      var updateFlag = { $inc: {'visitCount': Number(1)}};

//
      await dbo.collection("feedbacks").updateMany({billNumber: Number(req.body.billNumber), simpleDate: req.body.simpleDate, currentDate: req.body.currentDate}, {$set:{restaurantName: req.body.restaurantName, tableNumber: Number(req.body.tableNumber), captainName: req.body.captainName, waiterName: req.body.waiterName, customerName: req.body.customerName, customerMobile: req.body.customerMobile, email: req.body.email, dateOfBirth: req.body.dateOfBirth, dateOfAnn: req.body.dateOfAnn, ambiancePoints: Number(req.body.ambiancePoints), ambianceRemarks: req.body.ambianceRemarks, foodQualityPoints: Number(req.body.foodQualityPoints), foodQualityRemarks: req.body.foodQualityRemarks, customerServicePoints: Number(req.body.customerServicePoints), customerServiceRemarks: req.body.customerServiceRemarks, staffBehaviourPoints: Number(req.body.staffBehaviourPoints), staffBehaviourRemarks: req.body.staffBehaviourRemarks, valueForMoneyPoints: Number(req.body.valueForMoneyPoints), valueForMoneyRemarks: req.body.valueForMoneyRemarks, suggestion: req.body.suggestion, coinFlag: req.body.coinFlag, date: new Date((new Date().getTime()))}}, {upsert: true}, function(err, result) {

        if (err) {
          console.log(err);
        } else {
          console.log("1 document inserted");
          res.json({status: "success", message: "updated document", data: req.body.billNumber});
          db.close();
        }

      });
      //adding in dayFeedbacksSchema
//      dbo.collection("dayFeedback").updateMany({billNumber: Number(req.body.billNumber), simpleDate: req.body.simpleDate, currentDate: req.body.currentDate}, {
      await dbo.collection("dayFeedback").updateMany({billNumber: Number(req.body.billNumber), simpleDate: req.body.simpleDate, currentDate: req.body.currentDate}, {$set:{restaurantName: req.body.restaurantName, tableNumber: Number(req.body.tableNumber), captainName: req.body.captainName, waiterName: req.body.waiterName, customerName: req.body.customerName, customerMobile: req.body.customerMobile, email: req.body.email, dateOfBirth: req.body.dateOfBirth, dateOfAnn: req.body.dateOfAnn, ambiancePoints: Number(req.body.ambiancePoints), ambianceRemarks: req.body.ambianceRemarks, foodQualityPoints: Number(req.body.foodQualityPoints), foodQualityRemarks: req.body.foodQualityRemarks, customerServicePoints: Number(req.body.customerServicePoints), customerServiceRemarks: req.body.customerServiceRemarks, staffBehaviourPoints: Number(req.body.staffBehaviourPoints), staffBehaviourRemarks: req.body.staffBehaviourRemarks, valueForMoneyPoints: Number(req.body.valueForMoneyPoints), valueForMoneyRemarks: req.body.valueForMoneyRemarks, suggestion: req.body.suggestion, dayFlag: req.body.dayFlag, coinFlag: req.body.coinFlag, date: new Date((new Date().getTime()))}}, {upsert: true}, function(err, ress){
        if (err) {
          console.log(err);
        } else {
          console.log("1 dayFeedback added");
        //  db.customers.update({ mobile: req.body.customerMobile },{ $inc: { visitCount: 1 } });

          db.close();
        }
      });



      await dbo.collection("customers").updateMany({mobile: Number(req.body.customerMobile)}, { $inc: {visitCount: 1}}, {upsert: true}, function(error, resu){
        if (error) {
          console.log(error);
        } else{
          console.log("done");
          db.close();
        }
      });

      await dbo.collection("feedback_taken").updateMany({captainName: req.body.captainName, date: new Date(req.body.simpleDate)}, { $inc: {feedback_count:1}}, {upsert: true}, function(error, resultcount){
        if (error) {
          console.log(error);
        } else {
          console.log("incremented the feedback_taken Collection");
        }
      });

    });


  },


}
