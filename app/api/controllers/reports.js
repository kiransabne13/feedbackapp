const feedbackModel = require('../models/feedbacks');
var url = require('url');
var moment = require('moment');

module.exports = {

  getAll: function(req,res,next){
    let feedbackLists = [];
    console.log(req.query);
    console.log(req.headers);

    feedbackModel.find({}, function(err, feedbacks){

      if (err) {
        next(err);
      } else {
        for (let feedback of feedbacks){
          feedbackLists.push({id: feedback._id, restaurantName: feedback.restaurantName, tableNumber: feedback.tableNumber, billNumber: feedback.billNumber, captainName: feedback.captainName, waiterName: feedback.waiterName, customerName: feedback.customerName, customerMobile: feedback.customerMobile, email: feedback.email, dateOfBirth: feedback.dateOfBirth, dateOfAnn: feedback.dateOfAnn, ambiancePoints: feedback.ambiancePoints, ambianceRemarks: feedback.ambianceRemarks, foodQualityPoints: feedback.foodQualityPoints, foodQualityRemarks: feedback.foodQualityRemarks, customerServicePoints: feedback.customerServicePoints, customerServiceRemarks: feedback.customerServiceRemarks, staffBehaviourPoints: feedback.staffBehaviourPoints, staffBehaviourRemarks: feedback.staffBehaviourRemarks, valueForMoneyPoints:feedback.valueForMoneyPoints, valueForMoneyRemarks: feedback.valueForMoneyRemarks, suggestion: feedback.suggestion, date: feedback.date, coinFlag: feedback.coinFlag, simpledate: feedback.simpledate, currentDate: feedback.currentDate });
        }
        res.json({status:"success", message:"get All feedbacks", data: {feedbacks: feedbackLists}});
      }

    });

  }

}
