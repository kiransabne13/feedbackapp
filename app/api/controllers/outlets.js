const outletModel = require('../models/outlets');


module.exports = {
  getById: function(req,res,next){
    console.log(req.body);
    outletModel.findById(req.params.outletId, function(err, outletInfo ){
      if (err) {
        next(err);
      } else {
        res.json({status:"success", message: "Found Outlet Successful", data: {outlets: outletInfo}});
      }
    });
  },


  getAll: function(req,res,next){
    let outletsList = [];

    outletModel.find({}, function(err, outlets){

      if (err) {
        next(err);
      } else {
        for (let outlet of outlets) {
          outletsList.push({id: outlet._id, name: outlet.name, created_on: outlet.created_on, waiterCount: outlet.waiterCount, captainCount: outlet.captainCount});

        }
        res.json({status:"success", message: "Get All Outlets", data: {outlets: outletsList}});

      }
    });
  },


  updateById: function(req,res,next){
    outletModel.findByIdAndUpdate(req.params.outletId, {name:req.body.name}, function(err, outletInfo){

    if (err) {
      next(err);
    }  else {
      res.json({status: "success", message: "Outlet Update Successful", data: null});

      }
    });
  },


  deleteById: function(req,res,next){
    outletModel.findByIdAndRemove(req.params.outletId, function(err, outletInfo){
      if (err) {
          next(err);
      } else {
        res.json({status:"success", message: "deleted successgully", data: null});
      }
    });
  },


  create: function(req,res,next){
      outletModel.create({name: req.body.name, created_on: req.body.created_on, waiterCount: req.body.waiterCount, captainCount: req.body.captainCount}, function (err, result){
        if (err) {
            next(err);
        } else {
          res.json({status: "success", message: "Outlet Created", data: null});
        }
      });
  },


}
