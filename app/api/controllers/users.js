const userModel = require('../models/users');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

module.exports = {

  create: function(req,res,next){
    userModel.create({name: req.body.name, email: req.body.email, password: req.body.password, mobile:req.body.mobile, tokenId:req.body.tokenId, ownerId: req.body.ownerId, outletCount: req.body.outletCount, roles: req.body.roles }, function(err, result){
      if (err) {
          next(err);
      } else {
        res.json({status: "success", message: "User Registration Successful", data: null});
      }
    });
  },

  //authentication of users

  authenticate: function(req,res,next){
    userModel.findOne({email: req.body.email}, function(err, userInfo){
      if (err) {
        next(err);
      } else {

        if (bcrypt.compareSync(req.body.password, userInfo.password)) {
          const token = jwt.sign({id: userInfo._id}, req.app.get('secretKey'));

          res.json({status: "Authentication success", message: "user found!!!", data: {user: userInfo, token: token}});

        }else {
            res.json({status:"error", message: "Invalid email/password", data: null});
        }

      }
    });
  },


}
