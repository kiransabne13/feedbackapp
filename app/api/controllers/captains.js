const captainModel = require('../models/captains');

module.exports = {

  getById: function(req,res,next){
    console.log(req.body);
    captainModel.findById(req.params.captainId, function(err, captainInfo){
      if (err) {
        next(err);
      } else {
        res.json({status: "Success", message:" Found Captain", data: {captains: captainInfo}});
      }
    });
  },

  getAll: function(req,res,next){
    let captainLists = [];

    captainModel.find({}, function(err, captains){
      if (err) {
        next(err);
      } else {
        for (let captain of captains){
          captainLists.push({id: captain._id, name: captain.captainName, captainNumber: captain.captainNumber, restaurantName: captain.restaurantName });
        }
        res.json({status:"success", message:"get All captain", data: {captains: captainLists}});
      }
    });

  },

  updateById: function(req,res,next){
    captainModel.findByIdAndUpdate(req.params.captainId, {name:req.body.captainName}, function(err, captainInfo){
      if (err) {
        next(err);
      } else {
        res.json({status: "success", message: "captain Update Successful", data: null});
      }
    });
  },

  deleteById: function(req,res,next){
    captainModel.findByIdAndRemove(req.params.captainId, function(err, captainInfo){
      if (err){
        next(err);
      } else {
        res.json({status: "success", message: "deleted by Id", data: null});
      }
    });
  },

  create: function(req,res,next){
    captainModel.create({captainName: req.body.captainName, restaurantName: req.body.restaurantName, captainNumber: req.body.captainNumber}, function(err, result){
      if (err) {
        next(err);
      } else {
        res.json({status: "success", message: "captain Added", data: {id: result._id, captainName: result.captainName}});
      }
    });
  },


}
